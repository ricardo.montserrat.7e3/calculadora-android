package com.example.hacedordeoperacionesmatematicascalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Debug;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Struct;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText userNums;
    double userValue;
    int lastOperation;
    boolean usedDot = false;

    private boolean isEmpty()
    {
        return String.valueOf(userNums.getText()).isEmpty();
    }

    @Override
    public void onClick(View v) {
        Button button = (Button) v;

        switch (button.getId()) {
            case R.id.buttonDiv:
                if(isEmpty())
                    Toast.makeText(MainActivity.this, "There is no value to operate with",Toast.LENGTH_SHORT).show();
                else
                {
                    if(Double.isNaN(userValue))
                        userValue = Double.parseDouble(String.valueOf(userNums.getText()));
                    else
                        userValue /= Double.parseDouble(String.valueOf(userNums.getText()));

                    lastOperation = button.getId();
                    userNums.setText("");
                    usedDot = false;
                }
                break;
            case R.id.buttonMulti:
                if(isEmpty())
                    Toast.makeText(MainActivity.this, "There is no value to operate with",Toast.LENGTH_SHORT).show();
                else
                {
                    if(Double.isNaN(userValue))
                        userValue = Double.parseDouble(String.valueOf(userNums.getText()));
                    else
                        userValue *= Double.parseDouble(String.valueOf(userNums.getText()));

                    lastOperation = button.getId();
                    userNums.setText("");
                    usedDot = false;
                }
                break;
            case R.id.buttonSub:
                if(isEmpty())
                    Toast.makeText(MainActivity.this, "There is no value to operate with",Toast.LENGTH_SHORT).show();
                else
                {
                    if(Double.isNaN(userValue))
                        userValue = Double.parseDouble(String.valueOf(userNums.getText()));
                    else
                        userValue -= Double.parseDouble(String.valueOf(userNums.getText()));

                    lastOperation = button.getId();
                    userNums.setText("");
                    usedDot = false;
                }
                break;
            case R.id.buttonAdd:
                if(isEmpty())
                    Toast.makeText(MainActivity.this, "There is no value to operate with",Toast.LENGTH_SHORT).show();
                else
                {
                    if(Double.isNaN(userValue))
                        userValue = Double.parseDouble(String.valueOf(userNums.getText()));
                    else
                        userValue += Double.parseDouble(String.valueOf(userNums.getText()));

                    lastOperation = button.getId();
                    userNums.setText("");
                    usedDot = false;
                }
                break;
            case R.id.buttonEqual:
                switch (lastOperation)
                {
                    case R.id.buttonDiv:
                        userValue /= Double.parseDouble(String.valueOf(userNums.getText()));
                        break;
                    case R.id.buttonMulti:
                        userValue *= Double.parseDouble(String.valueOf(userNums.getText()));
                        break;
                    case R.id.buttonAdd:
                        userValue += Double.parseDouble(String.valueOf(userNums.getText()));
                        break;
                    case R.id.buttonSub:
                        userValue -= Double.parseDouble(String.valueOf(userNums.getText()));
                        break;
                }
                userNums.setText(String.valueOf(userValue));
                break;
            case R.id.buttonC: userNums.setText(""); userValue = Double.NaN; usedDot = false; break;
            case R.id.buttonDot:
                if(!usedDot)
                {
                    userNums.append(button.getText());
                    usedDot = true;
                }
                break;
            default: userNums.append(button.getText()); break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userNums = findViewById(R.id.numbers);
        userValue = Double.NaN;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Button c = findViewById(R.id.buttonC);
        Button dot = findViewById(R.id.buttonDot);
        Button button0 = findViewById(R.id.button0);
        Button button1 = findViewById(R.id.button1);
        Button button2 = findViewById(R.id.button2);
        Button button3 = findViewById(R.id.button3);
        Button button4 = findViewById(R.id.button4);
        Button button5 = findViewById(R.id.button5);
        Button button6 = findViewById(R.id.button6);
        Button button7 = findViewById(R.id.button7);
        Button button8 = findViewById(R.id.button8);
        Button button9 = findViewById(R.id.button9);

        Button div = findViewById(R.id.buttonDiv);
        Button multi = findViewById(R.id.buttonMulti);
        Button add = findViewById(R.id.buttonAdd);
        Button sub = findViewById(R.id.buttonSub);
        Button equal = findViewById(R.id.buttonEqual);

        button0.setOnClickListener(this);
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        button5.setOnClickListener(this);
        button6.setOnClickListener(this);
        button7.setOnClickListener(this);
        button8.setOnClickListener(this);
        button9.setOnClickListener(this);
        c.setOnClickListener(this);
        dot.setOnClickListener(this);

        div.setOnClickListener(this);
        multi.setOnClickListener(this);
        sub.setOnClickListener(this);
        add.setOnClickListener(this);
        equal.setOnClickListener(this);
    }
}